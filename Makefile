SHELL:=/usr/bin/env bash

.DEFAULT_GOAL := default

.PHONY: setup
setup:
	./bin/setup.sh

.PHONY: quality
quality:
	./bin/quality.sh

.PHONY: test
test:
	./bin/test.sh --debug --meld

.PHONY: package
package:
	./bin/package.sh

.PHONY: default
default: quality test
