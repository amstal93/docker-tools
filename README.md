
# NDD Docker Tools

Scripts and stuff about Docker.

<!-- MarkdownTOC -->

1. [Container entrypoint](#container-entrypoint)
    1. [Requirements](#requirements)
    1. [Installation](#installation)
    1. [Usage](#usage)

<!-- /MarkdownTOC -->



<a id="container-entrypoint"></a>
## Container entrypoint

A common problem when mounting volumes is the user on the host not being able to access the files created inside the container because the owner is someone else, usually `root`.

Files created inside a container belongs to the owner of the process inside this container.
But only the UID is shared between the Docker container and the Docker host.
So if these files are stored on a volume exposed from the host, that may be problematic because they may not belong to the host user.
These files may have another owner (often `root`, or even a non existing owner).

This Docker entrypoint executes the command with a user whose UID has been specified by the host.

<a id="requirements"></a>
### Requirements

This script has been tested with with the following Docker images:

- [Debian 10.7](https://hub.docker.com/_/debian)
- [Alpine 3.12](https://hub.docker.com/_/alpine)
- [Ubuntu 20.04](https://hub.docker.com/_/ubuntu)

This script has been tested with Bash 5.x.
You can display your Bash version `bash -c 'echo "${BASH_VERSION}"'`.

This script also requires `gosu` (for Debian and Ubuntu) or `su-exec` (for Alpine).

<a id="installation"></a>
### Installation

[Download a release](https://gitlab.com/ddidier/docker-tools/-/releases) and extract the `entrypoint/files/` directory alongside your `Dockerfile`.
This directory contains the main script `files/usr/local/bin/entrypoint` and its dependencies in `files/usr/share/ddidier/`:

```
├── Dockerfile
└── files
    └── usr
        ├── local
        │   └── bin
        │       └── docker-entrypoint
        └── share
            └── ddidier
                └── ...
```

With Debian and Ubuntu, you must at least install `gosu`:

```docker
RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install -y --no-install-recommends gosu \
 && apt-get autoremove -y \
 && rm -rf /var/cache/* \
 && rm -rf /var/lib/apt/lists/*
```

With Alpine, you must at least install `su-exec`:

```docker
RUN apk add --no-cache bash su-exec
```

For all of them, you must then copy the provided files and change the entrypoint:

```docker
COPY files/usr/local/  /usr/local/
COPY files/usr/share/  /usr/share/

RUN chown root:root /usr/local/bin/docker-entrypoint \
 && chmod 755       /usr/local/bin/docker-entrypoint

ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
```

<a id="usage"></a>
### Usage

In order for the user inside the container to match the user on the host, pass the environment variable `${USER_ID}`:

```bash
docker run -it --rm -v "${PWD}":/data -e USER_ID="${UID}" some-image some-command
```

The entrypoint will then:

- try to create a user with the specified UID if it doesn't exist
- execute with the specified UID the script `/usr/local/bin/docker-entrypoint-pre` if present
- execute with the specified UID the given command
- execute with the specified UID the script `/usr/local/bin/docker-entrypoint-post` if present

In order to debug the entrypoint, set the environment variable `${NDD_LOG4B_STDOUT_LEVEL}` to `DEBUG`:

```bash
docker run -it --rm -v "${PWD}":/data -e USER_ID="${UID}" -e NDD_LOG4B_STDOUT_LEVEL="DEBUG" some-image some-command
```
