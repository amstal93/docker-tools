#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_boolean  "meld"   false  "Open Meld on comparison failures"  "m"
DEFINE_boolean  "debug"  false  "Enable debug mode"                 "d"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



# ==============================================================================

# ------------------------------------------------------------------------------
function oneTimeSetUp() {

  DOCKER_IMAGE_NAME_PREFIX="ddidier/docker-tools/entrypoint-test"

  EXPECTED_DIR="${PROJECT_DIR}/tests/entrypoint/expected"

  EXISTING_USER_ID="9999"
  EXISTING_USER_NAME="existing-user"
  # ANOTHER_EXISTING_USER_ID="9998"
  ANOTHER_EXISTING_USER_NAME="another-existing-user"
  NON_EXISTING_USER_ID="12345"
  NON_EXISTING_USER_NAME="non-existing-user"


  log debug "Building all Docker images"

  build_docker_image "alpine"
  build_docker_image "alpine-with-container-user"
  build_docker_image "alpine-pre-post"

  build_docker_image "debian"
  build_docker_image "debian-with-container-user"
  build_docker_image "debian-pre-post"

  build_docker_image "ubuntu"
  build_docker_image "ubuntu-with-container-user"
  build_docker_image "ubuntu-pre-post"


  log debug "Creating temporary directory"

  TEMP_DIR=$(mktemp -d --suffix=.docker-tools)
  TEMP_DIR_LINK="$(dirname "${TEMP_DIR}")/docker-tools"

  rm -rf "${TEMP_DIR_LINK}"
  ln -sf "${TEMP_DIR}" "${TEMP_DIR_LINK}"
}



# ==============================================================================

# ------------------------------------------------------------------------------
function test_logs_without_user_id_and_without_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" ""
}

# ------------------------------------------------------------------------------
function test_logs_with_non_existing_user_id_and_without_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${NON_EXISTING_USER_ID}"
}

# ------------------------------------------------------------------------------
function test_logs_with_existing_user_id_and_without_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${EXISTING_USER_ID}"
}

# ------------------------------------------------------------------------------
function test_logs_with_non_existing_user_id_and_non_existing_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${NON_EXISTING_USER_ID}" "-e" "USER_NAME=${NON_EXISTING_USER_NAME}"
}

# ------------------------------------------------------------------------------
function test_logs_with_non_existing_user_id_and_existing_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${NON_EXISTING_USER_ID}" "-e" "USER_NAME=${EXISTING_USER_NAME}"
}

# ------------------------------------------------------------------------------
function test_logs_with_existing_user_id_and_non_existing_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${EXISTING_USER_ID}" "-e" "USER_NAME=${NON_EXISTING_USER_NAME}"
}

# ------------------------------------------------------------------------------
function test_logs_with_existing_user_id_and_corresponding_existing_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${EXISTING_USER_ID}" "-e" "USER_NAME=${EXISTING_USER_NAME}"
}

# ------------------------------------------------------------------------------
function test_logs_with_existing_user_id_and_different_existing_user_name() {
  do_test_with_all_images "${FUNCNAME[0]}" "" "-e" "USER_ID=${EXISTING_USER_ID}" "-e" "USER_NAME=${ANOTHER_EXISTING_USER_NAME}"
}



# ------------------------------------------------------------------------------
function test_logs_with_non_existing_user_id_and_without_user_name_but_container_user() {
  do_test_with_all_images "${FUNCNAME[0]}" "-with-container-user" "-e" "USER_ID=${NON_EXISTING_USER_ID}"
}

# ------------------------------------------------------------------------------
function test_logs_with_existing_user_id_and_without_user_name_but_container_user() {
  do_test_with_all_images "${FUNCNAME[0]}" "-with-container-user" "-e" "USER_ID=${EXISTING_USER_ID}"
}



# ------------------------------------------------------------------------------
function test_pre_and_post_process() {
  do_test_with_all_images "${FUNCNAME[0]}" "-pre-post"
}



# ==============================================================================

# ------------------------------------------------------------------------------
function build_docker_image() {
  local image_name_suffix="${1}"
  local image_name="ddidier/docker-tools/entrypoint-test-${image_name_suffix}"

  log debug "Building Docker test image ${image_name}"

  log debug "Creating temporary directory"

  local build_temp_dir
  local build_temp_dir_link
  build_temp_dir=$(mktemp -d --suffix=.docker-tools)
  build_temp_dir_link="$(dirname "${build_temp_dir}")/docker-tools"

  rm -rf "${build_temp_dir_link}"
  ln -sf "${build_temp_dir}" "${build_temp_dir_link}"

  log debug "Docker data will be stored in the temporary directory: ${build_temp_dir}"
  log debug "Use the following link for convenience: ${build_temp_dir_link}"

  log debug "Copying Docker files"
  cp -r "${PROJECT_DIR}/src/entrypoint/files" "${build_temp_dir}/"
  cp -r "${PROJECT_DIR}/tests/entrypoint/dockerfiles/Dockerfile-${image_name_suffix}" "${build_temp_dir}/Dockerfile"

  log debug "Building Docker image"

  log debug "$(ndd::print::script_output_start)"

  while read -r line; do
    log debug "${line}"
  done < <( docker build --tag "${image_name}" "${build_temp_dir}" )

  log debug "$(ndd::print::script_output_end)"
}

# ------------------------------------------------------------------------------
function do_test_with_all_images() {
  local test_name="${1}"
  local docker_image_name_suffix="${2}"
  local docker_environment_arguments=${*:3}

  do_test_with_and_without_debug "${test_name}" "alpine${docker_image_name_suffix}" "${docker_environment_arguments[@]}"
  do_test_with_and_without_debug "${test_name}" "debian${docker_image_name_suffix}" "${docker_environment_arguments[@]}"
  do_test_with_and_without_debug "${test_name}" "ubuntu${docker_image_name_suffix}" "${docker_environment_arguments[@]}"
}

# ------------------------------------------------------------------------------
function do_test_with_and_without_debug() {
  local test_name="${1}"
  local docker_image_name_suffix=${2}
  local docker_environment_arguments=${*:3}

  do_test "${test_name}_with_debug"    "${docker_image_name_suffix}" "${docker_environment_arguments[@]}" "-e" "NDD_LOG4B_STDOUT_LEVEL=DEBUG"
  do_test "${test_name}_without_debug" "${docker_image_name_suffix}" "${docker_environment_arguments[@]}"
}


# ------------------------------------------------------------------------------
function do_test() {
  local test_name="${1}"
  local docker_image_name_suffix=${2}
  local docker_environment_arguments=${*:3}

  local actual_log_file="${TEMP_DIR}/${test_name}.log"
  local expected_log_file="${EXPECTED_DIR}/${docker_image_name_suffix}/${test_name}.log"

  log debug "$(ndd::print::script_output_start)"

  while read -r line; do
    log debug "${line}"
  done < <(
    # shellcheck disable=SC2068
    docker run --rm                       \
      ${docker_environment_arguments[@]}  \
      "${DOCKER_IMAGE_NAME_PREFIX}-${docker_image_name_suffix}" id          \
    | tee "${actual_log_file}"
  )

  log debug "$(ndd::print::script_output_end)"

  assert_files_are_equal "${actual_log_file}" "${expected_log_file}"
}
# ------------------------------------------------------------------------------
function normalize_log_file() {
  local log_file="${1}"

  local current_year
  current_year=$(date "+%Y")
  sed -E -i 's/'"${current_year}"'-[[:digit:]]{2}-[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}/_DATE_/g' "${log_file}"

  sed -E -i 's/(Creating user '\''container-user-)[[:digit:]]*('\'' with UID)/\1_RANDOM_ID_\2/g' "${log_file}"

  sed -E -i 's/\s*$//g' "${log_file}"
}

# ------------------------------------------------------------------------------
function assert_files_are_equal() {
  local actual_log_file="${1}"
  local expected_log_file="${2}"
  local delta

  if [[ ! -f "${expected_log_file}" ]]; then
    fail "Missing file: ${expected_log_file}"
  fi

  normalize_log_file "${actual_log_file}"

  delta=$(diff "${actual_log_file}" "${expected_log_file}" || true)

  if [[ "${delta}" != "" ]]; then

    log error "Found differences between generated and expected files"
    log debug "$(ndd::print::script_output_start)"
    log debug "${delta}"
    log debug "$(ndd::print::script_output_end)"
    log error "Some useful commands:"
    log error "  diff '${actual_log_file}' '${expected_log_file}'"
    log error "  meld '${actual_log_file}' '${expected_log_file}' &"

    if [[ "${FLAGS_meld}" -eq "${FLAGS_TRUE}" ]] && command -v meld; then
        meld "${actual_log_file}" "${expected_log_file}" &
    fi

    fail "Failure in ${FUNCNAME[1]}"
  fi

}



# ==============================================================================

# ------------------------------------------------------------------------------
function main() {

  if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
    ndd::logger::set_stdout_level "DEBUG"
  else
    ndd::logger::set_stdout_level "INFO"
  fi

  # shellcheck disable=SC1090
  source "${PROJECT_DIR}/lib/shunit2/shunit2"
}

# ------------------------------------------------------------------------------
function error_handler() {
  local error_code="$?"

  test $error_code == 0 && return;

  log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

  exit 1
}

# ------------------------------------------------------------------------------
trap 'error_handler ${?}' ERR

main "${@}"

exit 0
